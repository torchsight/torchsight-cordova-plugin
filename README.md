cordova-torchclient
===================

Cordova interface for torch client.

Installation
------------

    cordova plugin add https://bitbucket.org/torchsight/torchsight-cordova-plugin.git

Supported Platforms
-------------------

- Android

Methods
-------

- torchclient.register(userid)


/**
 * Author: Brijesh.
 * Project: cordova-torchclient
 *
 */
package com.torchsight.torch;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.net.http.AndroidHttpClient;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.Settings.Secure;
import android.util.Log;


public class TorchClient extends CordovaPlugin {

	private static final String TAG = "TorchClient";
	private static final String PROPERTY_REG_ID = "registration_id";

	private CallbackContext callbackContext;
	private TorchServer mTorchServer;

	public TorchClient() {
	}

	@Override
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		super.initialize(cordova, webView);
		mTorchServer = new TorchServer(cordova.getActivity().getApplicationContext());
	}

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
		if (action.equals("register")) {
			
			this.callbackContext = callbackContext;
			String userId = null;
			try{
				userId = args.getString(0);
			}catch(Exception ex){
			}

			if (userId == null){
				this.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, ""));
			}else{
				mTorchServer.register(userId);
			}
		}else {
			return false;
		}

		this.sendPluginResult(new PluginResult(PluginResult.Status.NO_RESULT, ""));
		return true;
	}

	@Override
	public void onDestroy() {
		mTorchServer.cancel();
	}

	@Override
	public void onReset() {
	}

	private PluginResult sendPluginResult(PluginResult res) {
		res.setKeepCallback(true);
		this.callbackContext.sendPluginResult(res);
		return res;
	}
}

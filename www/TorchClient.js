/**
 * Author: Brijesh.
 * Project: cordova-torchclient
 *
 * JavaScript interface to the native torchclient
 */

// Tells native to start.
function register(userid) {
	if (userid != null){
		cordova.exec(
			function(res) {
			}, function(e) {
			}, "TorchClient", "register", [userid]);

    }
}

var torchclient = {
    register: function(userid) {
		register(userid);
    }
};
module.exports = torchclient;


